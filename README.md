# Demo Project `deployctl`

Purpose is to test releases and dynamic environment releases/downloads and Repositories of [deployctl](https://www.deployctl.com).

This project has no meaninfull source code, output binary will only print "hello world", for different architectures and creates packages and deploy pakages for rpm and deb - based distributions.

## Requirements

### projects: 
*  [deployctl/repo_hello_world](https://gitlab.com/deployctl/repo_hello_world)
*  [deployctl/bleeding_repo_hello_world](https://gitlab.com/deployctl/bleeding_repo_hello_world)

these are 2 projects defining and configuring 2 repositories.

### deployment server:

a server with deployctl setup and running with domainname and dns setup, example see [setup dployctl on DO](https://www.deployctl.com/2017/03/24/Setup-deployctl-on-DO.html)


## output:

1. on commit of branches, a review download page will be generated, see [environments](https://gitlab.com/deployctl/heloworld_c/environments) for url.
2. on commit of the master, a [master](http://master.deployctl-heloworld-c.gioxapp.com) review page and packages are pushed to the [bleeding_repo_helloworld](http://production.deployctl-bleeding-repo-hello-world.gioxapp.com).
3. on tag commit (create tag) a release preview page [staging](http://staging.deployctl-heloworld-c.gioxapp.com) is created, containing the release notes as per tag.

From staging, manual intervention is needed to deploy to production.

On deploy to production the [release page](https://hello-world.deployctl.com/latest) is created and packages pushed to [helloworld-repository](http://production.deployctl-repo-hello-world.gioxapp.com)
additionally a release_tag is created:

[![](https://hello-world.deployctl.com/tag.svg)](https://hello-world.deployctl.com/latest/)


